# NodeJS Command Line Weather App

## Getting Started
```shell
$ npm install
$ node app.js --address "Some address"
```

An example usage:
```shell
$ node app.js --address "122 Fake Street, Dublin"

122 Fake Street, Fake Town, Dublin 12, Ireland
Currently, it is 22°C but it feels like 24.8°C.
```


## Command line options
`--address`, `-a`: The address to check for weather information

`--help`: List available command line options