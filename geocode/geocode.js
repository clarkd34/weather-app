const request = require('request');
const googleMapsAPIKey = 'AIzaSyD88wmGHBYK-IOpHwt6AoKmuiRKkYqAUzY';

const geocodeAddress = address => {
  const encodedAddress = encodeURIComponent(address);
  return new Promise((resolve, reject) => {
    request({
      url: `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}&key=${googleMapsAPIKey}`,
      json: true
    }, (error, response, body) => {
      if(error) {
        reject('Unable to connect to Google servers.');
      }
      else if(body.status === 'ZERO_RESULTS') {
        reject('Unable to find that address.');
      }
      else if(body.status === 'QUERY_LIMIT_EXCEEDED') {
        reject(body.status);
      }
      else if(body.status === 'OK') {
        resolve({
          address: body.results[0].formatted_address,
          latitude: body.results[0].geometry.location.lat,
          longitude: body.results[0].geometry.location.lng
        });
      }
    });
  });
};

module.exports = {
  geocodeAddress
};