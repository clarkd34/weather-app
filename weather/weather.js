const request = require('request');
const darkSkyAPIKey = '15a816998c7d1aa96c2c532bbd36295d';

const getWeather = (lat, lon, callback) => {
  request({
    url: `https://api.darksky.net/forecast/${darkSkyAPIKey}/${lat},${lon}`,
    json: true
  }, (error, response, body) => {
    if(!error && response.statusCode === 200) {
      callback(undefined, {
        temperature: (body.currently.temperature - 32).toPrecision(2),
        feelsLike: (body.currently.apparentTemperature - 32).toPrecision(2)
      });
    }
    else {
      callback('Cannot fetch weather data');
    }
  });
};

module.exports = {
  getWeather
};