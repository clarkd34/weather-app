const yargs = require('yargs');
const axios = require('axios');

const argv = yargs
  .options({
    a: {
      alias: 'address',
      describe: 'Address to fetch weather for',
      demand: true,
      string: true,
    },
  })
  .help()
  .alias('help', 'h')
  .argv;

const encodedAddress = encodeURIComponent(argv.address);
const googleMapsAPIKey = 'AIzaSyD88wmGHBYK-IOpHwt6AoKmuiRKkYqAUzY';
const darkSkyAPIKey = '15a816998c7d1aa96c2c532bbd36295d';

const geocodeURL = `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}&key=${googleMapsAPIKey}`;
axios.get(geocodeURL)
  .then(response => {
    if(response.data.status === 'ZERO_RESULTS') {
      throw new Error('Unable to find that address');
    }

    const lat = response.data.results[0].geometry.location.lat;
    const lng = response.data.results[0].geometry.location.lng;
    const weatherURL = `https://api.darksky.net/forecast/${darkSkyAPIKey}/${lat},${lng}`;
    console.log(response.data.results[0].formatted_address);

    return axios.get(weatherURL);
  })
  .then(response => {
    const fToC = (f, scale) => (f - 32).toPrecision(scale || 2);
    const temperature = fToC(response.data.currently.temperature);
    const feelsLike = fToC(response.data.currently.apparentTemperature);
    console.log(`Currently, it is ${temperature}°C but it feels like ${feelsLike}°C.`)
  })
  .catch(error => {
    if(error.code === 'ENOTFOUND') {
      console.log('Unable to connect to API servers.');
    }
    else {
      console.log(error.message);
    }
  });
