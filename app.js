const yargs = require('yargs');
const geocode = require('./geocode/geocode');
const weather = require('./weather/weather');

const argv = yargs
  .options({
    a: {
      alias: 'address',
      describe: 'Address to fetch weather for',
      demand: true,
      string: true,
    },
  })
  .help()
  .alias('help', 'h')
  .argv;

const address = argv.address;
geocode.geocodeAddress(address)
  .then(({ address, latitude, longitude }) => {
    return new Promise((resolve, reject) => {
      weather.getWeather(
        latitude,
        longitude,
        (error, { temperature, feelsLike }) => {
          if (error) {
            reject(error);
          }
          else {
            resolve({
              address: address,
              message: `Currently, it is ${temperature}°C but it feels like ${feelsLike}°C.`
            });
          }
        });
    });
  })
  .then(({ address, message }) => {
    console.log(address);
    console.log(message);
  })
  .catch(error => {
    console.log(error);
  });
